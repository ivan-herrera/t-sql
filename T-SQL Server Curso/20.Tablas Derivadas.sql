SELECT 
	*
FROM sales.customers as CS;

--INSERT INTO sales.customers(customer_id,first_name)
SELECT 
	Id,
	First_Name
FROM --Nombre de Objeto
(-- Subconsulta
	VALUES(1,'Ivan'),(2,'Juan')
) as CS (Id,First_Name)

;
INSERT INTO sales.customers(customer_id,first_name)
VALUES(1,'Ivan'),(2,'Juan')

/** OBTENER UNA COLUMNA CON EL NOMBRE DEL ESTADO A PARTIR DE UNA TABLA DERIVADA **/
SELECT * 
FROM sales.customers c
INNER JOIN 
(
	VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
) AS s (state_name,state)
ON s.state = c.state
/**----------------------------------------**/
SELECT * 
FROM sales.customers c
INNER JOIN 
(
	SELECT 
		state_name,
		state
	FROM 
	(
		VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
	) AS states (state_name,state)
) as s
ON s.state = c.state

/***----------------------------------**/

SELECT 
	*,
	(SELECT state_name FROM 
		(
			VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
		) AS states (state_name,state)
	 WHERE states.state = c.state
	) as [state]
FROM sales.customers c

/** Obtener el resultado del reporte utilizando subconsultas **/
SELECT * 
FROM dbo.vw_reporte_de_ventas_por_estado



-- xxx INNER JOIN

SELECT * FROM sales.orders o
SELECT * FROM sales.order_items

/*** Paso 1: Recuperar valores*/

SELECT 
	customer_id, 
	(list_price*quantity*(1-discount)) as sub_total 
FROM sales.orders o INNER JOIN sales.order_items i
ON o.order_id = i.order_id

/*** Paso 2: Agrupar valores por customer_id */
SELECT 
	customer_id, 
	SUM(list_price*quantity*(1-discount)) as sub_total 
FROM sales.orders o INNER JOIN sales.order_items i
ON o.order_id = i.order_id
GROUP BY customer_id

/*** Paso 3: Agregar la subconsulta a la consulta principal*/
SELECT 
	state,
	(SELECT state_name FROM 
		(
			VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
		) AS states (state_name,state)
	 WHERE states.state = c.state
	) as [state_name],
	(
		--Crear consulta que recupere el subtotal
		SELECT 
			--customer_id, 
			SUM(list_price*quantity*(1-discount)) as sub_total 
		FROM sales.orders o INNER JOIN sales.order_items i
		ON o.order_id = i.order_id
		GROUP BY customer_id
	) as sub_total
FROM sales.customers c

/** Paso 4: Buscar valores con una subconsulta correlacionada */

SELECT 
	state,
	(SELECT state_name FROM 
		(
			VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
		) AS states (state_name,state)
	 WHERE states.state = c.state
	) as [state_name],
	(
		--Crear consulta que recupere el subtotal
		SELECT 
			--customer_id, 
			SUM(list_price*quantity*(1-discount)) as sub_total 
		FROM sales.orders o INNER JOIN sales.order_items i
		ON o.order_id = i.order_id
		-- Correlacion
		WHERE o.customer_id = c.customer_id
		GROUP BY customer_id
	) as sub_total
FROM sales.customers as c

/*** Paso 5: Agrupar sub_total por estado */

SELECT 
	state,
	state_name,
	SUM(sub_total) as sub_total
FROM (
	SELECT 
		state,
		(SELECT state_name FROM 
			(
				VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
			) AS states (state_name,state)
		 WHERE states.state = c.state
		) as [state_name],
		(
			--Crear consulta que recupere el subtotal
			SELECT 
				--customer_id, 
				SUM(list_price*quantity*(1-discount)) as sub_total 
			FROM sales.orders o INNER JOIN sales.order_items i
			ON o.order_id = i.order_id
			-- Correlacion
			WHERE o.customer_id = c.customer_id
			GROUP BY customer_id
		) as sub_total
	FROM sales.customers as c
) as t
GROUP BY state, state_name

