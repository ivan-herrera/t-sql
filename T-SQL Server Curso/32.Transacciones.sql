SELECT * FROM sales.budgets;

---DELETE, INSERT, UPDATE

--BEGIN TRAN
BEGIN TRANSACTION

UPDATE sales.budgets SET may = 30050 WHERE store_id = 0;
SELECT * FROM sales.budgets;

DELETE FROM sales.budgets;

SELECT  @@TRANCOUNT	
SELECT * FROM sales.budgets;


--COMMIT TRANSACTION --COMMIT TRAN
ROLLBACK TRANSACTION --ROLLBACK TRAN
SELECT * FROM sales.budgets;


