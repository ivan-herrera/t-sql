DECLARE @result as int;
DECLARE @numero as int =-1;
Begin Try

	IF(@numero < 0)
		RAISERROR (60000,16, 1,@numero,'@result');
		---Throw 50001,'Valor negativo',1
	SELECT @result = 10/@numero;
End Try
Begin Catch
    /** Almacenar **/
	/**
	Crear tabla bitacora
	Definir las columnas
	- Fecha del error
	Insertar valores
	*/
	INSERT INTO [dbo].[Bitacora]
           ([Number],[Message],[Procedure],[State],[Line])
	SELECT  ERROR_NUMBER(),ERROR_MESSAGE(),ERROR_PROCEDURE(),ERROR_STATE(),ERROR_LINE()
	SET @result =0;
End Catch

SELECT * FROM dbo.Bitacora;

SELECT 'Siguiente script', @result;


--SELECT * FROM sys.messages WHERE message_id = 60000


/**Creando mensaje personalizado**/
--EXEC sp_addmessage 60000,16,N'Valor %d Negativo, al trata de establecer valor %s',null,true





/*
CREATE TABLE dbo.Bitacora
	(
	Number int NOT NULL,
	Message nvarchar(100) NOT NULL,
	[Procedure] nvarchar(50) NULL,
	State int NOT NULL,
	Line int NOT NULL,
	Date datetime NOT NULL
	) 
GO


CREATE TABLE dbo.Bitacora
	(
	Number int NOT NULL,
	Message nvarchar(100) NOT NULL,
	[Procedure] nvarchar(50) NULL,
	State int NOT NULL,
	Line int NOT NULL,
	Date datetime NOT NULL  DEFAULT GETDATE()
	) 

GO
*/