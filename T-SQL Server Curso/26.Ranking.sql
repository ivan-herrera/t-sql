
SELECT 
	RANK() OVER(PARTITION BY store_id,brand_name Order By subtotal),
	RANK() OVER( Order By subtotal),
	DENSE_RANK()OVER( Order By subtotal),
	ROW_NUMBER() OVER( Order By subtotal),
	NTILE(8)  OVER( Order By subtotal),

* 
FROM dbo.vw_reporte_de_ventas_por_marca ORDER BY store_id,SubTotal



CREATE TABLE #tmp (id int, value int);

INSERT INTO #tmp
VALUES
(1,10),
(2,12),
(3,12),
(4,15),
(5,16),
(6,20),
(7,20),
(8,20),
(9,25),
(10,30),
(11,35),
(12,40)


SELECT 
	RANK()OVER(order by value desc ) as RANK,
	DENSE_RANK()OVER(order by value,id) as DENSE_RANK,
	ROW_NUMBER()OVER(order by value) as ROW_NUMBER,
	NTILE(5) OVER(order by value) as NTILE,
	* 
FROM #tmp