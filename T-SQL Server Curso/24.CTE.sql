/*** CTE - Insert ***/

CREATE TABLE #tmp (id int,value varchar(100));

WITH Cte AS  (
	SELECT staff_id,first_name FROM sales.staffs
)
INSERT INTO #tmp
SELECT * FROM Cte;

SELECT * FROM Cte;



/**** Recursiva *****/
;
WITH Cte AS  (
	SELECT staff_id,first_name,CAST(' ' as Varchar(50)) as manager_name,
		  1 as vuelta
	FROM sales.staffs
	WHERE manager_id IS NULL
		UNION ALL
	SELECT s.staff_id,s.first_name,c.first_name, c.vuelta +1	
	FROM Cte c
	INNER JOIN sales.staffs s ON c.staff_id = s.manager_id
)
SELECT * FROM Cte;


/*******/

SET LANGUAGE SPANISH --Cambiar configuracion del lenguaje
SELECT @@LANGUAGE --Consulta lenguaje de la coneccion

DECLARE @i as int = 0;
DECLARE @fecha as Date = DATEADD(dd,@i, DATEFROMPARTS(2022,1,1));

With dias AS (
	SELECT @i as dia, @fecha as fecha,
		DATENAME(dw,@fecha) as dia_semana
	UNION ALL
	SELECT dia+1,DATEADD(dd,1,fecha),DATENAME(dw,DATEADD(dd,1,fecha)) FROM dias
	WHERE dia+1<30
)
SELECT *,CAST(dia_semana as nvarchar(20)) FROM dias
OPTION (MAXRECURSION 400);











/*****/
SELECT 
	staff_id,
	first_name,
	--
	(SELECT first_name FROM sales.staffs b WHERE b.staff_id = a.manager_id)
	as manager_name,
	*
FROM sales.staffs a;

/** SELF JOIN***/
SELECT 
	a.staff_id,
	a.first_name,
	m.first_name
FROM sales.staffs a
LEFT JOIN
(SELECT first_name,staff_id FROM sales.staffs b)as m
ON a.manager_id = m.staff_id;