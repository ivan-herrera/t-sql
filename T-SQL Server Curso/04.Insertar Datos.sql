

SELECT * FROM T1;

/*Insertar Datos*/
INSERT INTO T1 
VALUES(1,'Valor 1');

INSERT INTO T1 
VALUES(2,NULL);

/**Insert con columnas explicitas*/
INSERT INTO T1(Id) 
VALUES(3);

INSERT INTO T1
VALUES('5',4);

INSERT INTO T1 (Value,Id)
VALUES('Dato',4);


SELECT * FROM T1;


/*** Ejemplo 2 ****/

INSERT INTO dbo.T2 (Id)
VALUES(1);

/** Error de Violacion de Llave Primaria**/
INSERT INTO dbo.T2 (Id)
VALUES(1);

/**Insertar valores nulos de manera explicita*/
INSERT INTO dbo.T2 (Id,Value)
VALUES(2,NULL);


INSERT INTO dbo.T2 (Id)
VALUES(3);

SELECT * FROM dbo.T2;

