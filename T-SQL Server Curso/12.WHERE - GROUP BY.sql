USE BikeStores

SELECT * 
FROM sales.orders
WHERE order_date = '2016-01-28' 
;

SELECT * FROM sales.orders
WHERE order_status = 4;

SELECT * FROM sales.orders 
WHERE order_status = 4 
AND YEAR(shipped_date) = 2016;

/**Ordenes con envio en el a�o 2016 y 2018**/
SELECT * FROM sales.orders
WHERE YEAR(shipped_date) = 2016 OR YEAR(shipped_date)=2018

/* Comprobar que solo hay fechas de envio 2016 y 2018*/
SELECT 
	YEAR(shipped_date) as  shipped_year,
	COUNT(order_date) as row_count
FROM sales.orders
WHERE YEAR(shipped_date) = 2016 
OR YEAR(shipped_date)=2018
GROUP BY YEAR(shipped_date);

/* Reporte de Ventas por Tienda*/
SELECT 
	--s.store_id,
	orders.store_id,
	s.store_name,
	CONVERT(VARCHAR,CAST(SUM(quantity*list_price*(1-discount)) AS MONEY) ,101) as SubTotal
FROM sales.orders 
INNER JOIN sales.order_items 
ON orders.order_id = order_items.order_id
LEFT JOIN sales.stores s
ON orders.store_id = s.store_id
GROUP BY orders.store_id,s.store_id,s.store_name


SELECT * FROM sales.stores

SELECT * FROM sales.order_items