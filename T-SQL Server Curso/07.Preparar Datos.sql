CREATE TABLE T1 (
	Id int NOT NULL Primary Key,
	Value Varchar(1) NOT NULL);



INSERT INTO T1
VALUES
(1,'A'),
(2,'B'),
(3,'C'),
(5,'F'),
(6,'G'),
(9,'J')

CREATE TABLE T2 (
	Id int NOT NULL Primary Key,
	Value Varchar(1) NOT NULL);

DELETE FROM T2;

INSERT INTO T2
VALUES
(2,'b'),
(3,'c'),
(4,'e'),
(5,'f'),
(6,'g'),
(8,'i')