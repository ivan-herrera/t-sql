
CREATE VIEW vw_reporte_de_ventas_por_marca
AS
SELECT 
	o.store_id,
	s.store_name,
	b.brand_id,
	b.brand_name,
	SUM(i.quantity*i.list_price*(1-i.discount)) as SubTotal
FROM sales.orders as  o
INNER JOIN sales.order_items as i
ON o.order_id = i.order_id
LEFT JOIN sales.stores s
ON o.store_id = s.store_id
LEFT JOIN production.products as p
ON i.product_id = p.product_id
LEFT JOIN production.brands b
ON b.brand_id = p.brand_id
GROUP BY o.store_id,s.store_id,s.store_name,b.brand_id,	b.brand_name
GO

CREATE VIEW vw_reporte_de_ventas_por_estado
AS
SELECT 
    s.state,

	CASE s.state
		WHEN 'CA' THEN 'CALIFORNIA'
		WHEN 'TX' THEN 'TEXAS'
		ELSE 'SIN ESTADO'
	END AS state_name, 
	CASE 
		WHEN s.state = 'CA' THEN 'CALIFORNIA'
		WHEN s.state = 'TX' THEN 'TEXAS'
	END AS state_name_v2,
	sum(SubTotal) as SubTotal
FROM 
dbo.vw_reporte_de_ventas_por_marca v
INNER JOIN sales.stores s
ON v.store_id = s.store_id
GROUP BY s.state;
GO


select * from vw_reporte_de_ventas_por_estado