/*
A = Ventas Mayores a 300,000
B = Ventas Menores a 300,000 y Mayores a 200,000
C = Ventas Menores a 200,000
*/
SELECT 
	o.store_id,
	s.store_name,
	b.brand_id,
	b.brand_name,
	SUM(i.quantity*i.list_price*(1-i.discount)) as SubTotal,
	CASE 
		WHEN SUM(i.quantity*i.list_price*(1-i.discount))>300000 THEN 'A'
		WHEN SUM(i.quantity*i.list_price*(1-i.discount))<=300000 
			AND SUM(i.quantity*i.list_price*(1-i.discount))>=200000 THEN 'B'
		ELSE 'C'
	END AS Clasificacion,
	CASE 
		WHEN SUM(i.quantity*i.list_price*(1-i.discount))>300000 THEN 'A'
		WHEN SUM(i.quantity*i.list_price*(1-i.discount)) BETWEEN 300000 
			AND 200000 THEN 'B'
		ELSE 'C'
	END AS Clasificacion
FROM sales.orders as  o
INNER JOIN sales.order_items as i
ON o.order_id = i.order_id
LEFT JOIN sales.stores s
ON o.store_id = s.store_id
LEFT JOIN production.products as p
ON i.product_id = p.product_id
LEFT JOIN production.brands b
ON b.brand_id = p.brand_id
GROUP BY o.store_id,s.store_id,s.store_name,b.brand_id,	b.brand_name
--HAVING SUM(i.quantity*i.list_price*(1-i.discount)) > 300000

SELECT 
	o.store_id,
	s.store_name,
	b.brand_id,
	b.brand_name,
	SUM(i.quantity*i.list_price*(1-i.discount)) as SubTotal
FROM sales.orders as  o
INNER JOIN sales.order_items as i
ON o.order_id = i.order_id
LEFT JOIN sales.stores s
ON o.store_id = s.store_id
LEFT JOIN production.products as p
ON i.product_id = p.product_id
LEFT JOIN production.brands b
ON b.brand_id = p.brand_id

GROUP BY o.store_id,s.store_id,s.store_name,b.brand_id,	b.brand_name
HAVING SUM(i.quantity*i.list_price*(1-i.discount)) <= 300000

SELECT 
	* ,
	CASE 
		WHEN SubTotal > 300000 THEN 'A'
		WHEN SubTotal BETWEEN 300000 AND 200000 THEN 'B'
		ELSE 'C'
	END AS Clasificacion
FROM
[dbo].[vw_reporte_de_ventas_por_marca];