/*** Procedimiento Almacenado () ***/

ALTER PROCEDURE dbo.MyProcedure (@in as int, @in2 as int, @out as int OUT)
AS
BEGIN
	SELECT @in as field;
	SELECT @in2 as field;

	SELECT  @out = @in * @in2;


END

GO

DECLARE @out_ as int = 0;

select @out_;

--EXEC dbo.MyProcedure 10,20, @out_ out;

select @out_;


EXEC dbo.MyProcedure @in2=20, @in= 10, @out= @out_ out;

GO


CREATE PROC dbo.Eliminar 
AS
BEGIN
	SELECT * FROM T1;
	DELETE FROM T1 WHERE id=1;
	DELETE TOP(2) FROM T1 WHERE Value='SIN DATO';
	DELETE FROM T1;
	SELECT * FROM T2;
	DELETE TOP(1) FROM T2 WHERE Value='SIN VALOR';
END



GO

ALTER PROC sales.CreateBudget 
	(@storeId as int,  @mes as int, @budget as money)
AS
BEGIN
	DECLARE @year as int = Year(GETDATE());

	DECLARE @sucursal as int = ISNULL((SELECT store_id FROM sales.stores  WHERE store_id = @storeId),0)

	INSERT INTO [sales].[budgets]
			   ([store_id],[ene],[feb],[year])
		 VALUES
			   (@sucursal,@ene,@feb,@year)
END
GO


EXEC sales.CreateBudget 20, 1000,20000

SELECT * FROM sales.budgets