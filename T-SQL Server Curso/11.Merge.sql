USE TestDB

SELECT * FROM T1;
SELECT * FROM T2;

MERGE T2 --Tabla a afectar
USING T1 -- Tabla Origen
ON T1.id = T2.id
WHEN MATCHED --AND T1.Status = 1 -- Cuando halla correspondencia
	THEN UPDATE SET value = LOWER(T1.value)
WHEN NOT MATCHED --Cuando(LEFT JOIN)
	THEN INSERT (id,value) 
	VALUES (T1.id,T1.value)
WHEN NOT MATCHED BY Source --Cuando (RIGHT JOIN)
	THEN DELETE
OUTPUT deleted.Id,deleted.value,inserted.value;

/**Equivalencia**/
INSERT INTO T2
SELECT id,value FROM T1
WHERE id in (1,9);

UPDATE T2
	SET Value = T1.Value
	FROM T2 INNER JOIN T1
	ON T1.Id = T2.Id

DELETE FROM T2
WHERE id in (8,4);