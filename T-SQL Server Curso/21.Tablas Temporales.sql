CREATE TABLE #states (
	state char(2),
	state_name varchar(30));

SELECT * FROM #states;

INSERT INTO #states (state_name,state)
VALUES('Nueva York','NY'),('Texas','TX'),('California','CA');


SELECT * 
FROM sales.customers c
INNER JOIN 
#states AS s 
ON s.state = c.state
/**----------------------------------------**/
SELECT * 
FROM sales.customers c
INNER JOIN 
(
	SELECT 
		state_name,
		state
	FROM #states
	--(
	--	VALUES('Nueva York','NY'),('Texas','TX'),('California','CA')
	--) AS states (state_name,state)
) as s
ON s.state = c.state;

--Eliminar tabla de la base de datos
--DROP TABLE #states;


/********** TABLA TEMPORAL GLOBAL ********/
CREATE TABLE ##states (
	state char(2),
	state_name varchar(30));

SELECT * FROM ##states;

INSERT INTO ##states (state_name,state)
VALUES('Nueva York','NY'),('Texas','TX'),('California','CA');
