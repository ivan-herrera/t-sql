DECLARE @var as CHAR(1) = NULL;
DECLARE @var2 as CHAR(1) = 'E';

DECLARE @varint as int =0;


SELECT 
	ISNULL(ISNULL(@var,@var2),'D'),
	COALESCE(@var,null,@var2,'D'),
	NULLIF(@var2,'D'),
	NULLIF(@var2,'F'),
	15/NULLIF(@varint,0)

/** Operador IS NULL **/
SELECT * 
FROM vw_reporte_de_ventas_por_marca
WHERE store_name IS NULL

SELECT
	CASE WHEN store_name IS NULL THEN 'SIN TIENDA' ELSE store_name END as store_name,
	IIF(store_name IS NULL , 'SIN TIENDA' , store_name) as store_name,
	*
FROM vw_reporte_de_ventas_por_marca
