
SELECT 
	CAST(store_id as varchar) + ':' + CAST(customer_id as varchar) as CLAVE,
	CONCAT(store_id,':',customer_id) as CLAVE,
	CONVERT(VARCHAR,CAST(customer_id as MONEY),3),
	FORMAT(customer_id,'C'),
	FORMAT(customer_id,'C','es-ES'),
	ISDATE(CAST(order_date as VARCHAR)) as esFecha,
	ISDATE('13-13-2020'),

	EOMONTH(order_date) as FinMes,
	DATETIMEFROMPARTS(2020,1,3,10,20,23,150) as Fecha,
	CHOOSE(4,'A','B','C','D') as Choose_select,
	Month(order_date) as Mes,
	CHOOSE(Month(order_date),'Ene','Feb','Mar','Abr') as Mes_Desc,
	IIF( Month(order_date) = 2, 'Verdadero','False' ) as esFebrero,

	
	*

FROM sales.orders


SELECT 
	PARSE('10-12-2022' as Date),
	TRY_PARSE('13-13-2022' as Date),
	ISNULL(TRY_PARSE('13-13-2022' as Date),'20201001'),
	ISNULL(TRY_PARSE('13-13-2022' as Date),GETDATE()),
	ISNULL(TRY_PARSE('13-13-2022' as Date),DATEFROMPARTS(1900,1,1)),
	ISDATE('13-13-2022'),
	NULLIF(ISDATE('13-13-2022'),0) as NULO,
	NULLIF(ISDATE('10-13-2022'),0) as NO_NULO,
	ISNULL(TRY_PARSE(NULLIF(ISDATE('13-13-2022'),0) as DATE),DATEFROMPARTS(2010,1,1)) as Fecha_ISDATE,
	--PARSE('13-13-2022' as Date)