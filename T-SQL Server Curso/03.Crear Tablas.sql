--CREATE DATABASE TestDB;
USE TestDB;
GO

SELECT DB_NAME()
GO

CREATE TABLE dbo.T1 (
	Id Int NOT NULL,
	Value Varchar(10) NULL
);

CREATE TABLE T2(
	Id Int NOT NULL Primary Key,
	Value Varchar(10) NULL Default 'SIN VALOR'
);