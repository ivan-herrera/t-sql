/** Subconsulta **/
SELECT 
	*
FROM sales.orders o
WHERE customer_id IN (
SELECT customer_id FROM sales.customers
WHERE state = 'CA')

/** Subconsulta correlacionada**/
SELECT 
	*
FROM sales.orders as o
WHERE EXISTS (
	SELECT customer_id FROM sales.customers as  c
	WHERE state = 'CA' 
	AND o.customer_id = c.customer_id --Dato Externo
)

/** Subconsulta en listado de select**/

SELECT 
*,
Format(SubTotal / (
	SELECT SUM(SubTotal) 
	FROM dbo.vw_reporte_de_ventas_por_estado)
,'##.#0 %') as [% Participacion]
FROM dbo.vw_reporte_de_ventas_por_estado

