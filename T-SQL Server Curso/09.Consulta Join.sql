SELECT 
	s.store_name as "store name",
	c.first_name + ' ' + c.last_name as "customer name",
	st.first_name + ' ' + st.last_name as [staff name],
	o.* 
FROM sales.orders as o
	INNER JOIN sales.stores as s
	ON o.store_id = s.store_id
	INNER JOIN sales.customers c
	ON c.customer_id = o.customer_id --Condicion de union
	INNER JOIN sales.staffs as st -- alias "st"
	ON st.staff_id = o.staff_id -- Condicion de union