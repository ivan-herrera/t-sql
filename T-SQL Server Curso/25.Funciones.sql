
/***** Crear Funciones escalares ****/

CREATE FUNCTION dbo.fnSuma (@A as int, @B as int)
RETURNS int
AS
BEGIN
	DECLARE @result as int = @A + @B;
	RETURN @result ;
END
GO

SELECT dbo.fnSuma(10,20);
GO
/** Crear Funcion Tabla ***/
CREATE FUNCTION dbo.fnClientes(@state as char(2))
RETURNS TABLE
AS
	Return (SELECT * FROM sales.customers WHERE state = @state);

GO

SELECT * FROM sales.orders o 
inner join dbo.fnClientes('TX') f
ON f.customer_id = o.customer_id;
GO
/********/
CREATE FUNCTION dbo.fnClientes_estado(@state as char(2))
RETURNS @return TABLE (customer_id int, firt_name varchar(50),last_name varchar(50),state varchar(20))
AS
BEGIN
    DECLARE @estados as TABLE (id varchar(2), name varchar(20));
	INSERT INTO @estados
	VALUES('TX','TEXAS'),('NY','NUEVA YORK'),('CA','CALIFORNIA');

	INSERT INTO @return 
	SELECT customer_id,first_name,last_name,name 
	FROM sales.customers c INNER JOIN  @estados e ON e.id = c.state
	WHERE state = @state;

	Return;
END
GO

CREATE FUNCTION dbo.fnNombreEstados()
RETURNS TABLE
AS
RETURN (SELECT id,name FROM ( VALUES('TX','TEXAS'),('NY','NUEVA YORK'),('CA','CALIFORNIA')) 
		as estados (id,name));

GO

CREATE FUNCTION dbo._fnNombreEstados()
RETURNS @estados  TABLE (id varchar(2), name varchar(20))
AS
BEGIN
	INSERT INTO @estados
	VALUES('TX','TEXAS'),('NY','NUEVA YORK'),('CA','CALIFORNIA');
	Return;
END
GO
/*** Refactorizacion ****/
/********/
ALTER FUNCTION dbo.fnClientes_estado(@state as char(2))
RETURNS @return TABLE (customer_id int, firt_name varchar(50),last_name varchar(50),state varchar(20))
AS
BEGIN
	INSERT INTO @return 
	SELECT customer_id,first_name,last_name,name 
	FROM sales.customers c INNER JOIN  dbo.fnNombreEstados() e ON e.id = c.state
	WHERE state = @state;

	Return;
END
GO


GO
SELECT * FROM fnClientes_estado('TX')

/**** Practica *****/

SELECT  nombre_completo,
	* 
FROM sales.orders o
CROSS APPLY sales.nombre_cliente_table(o.customer_id) f

GO

CREATE FUNCTION sales.nombre_cliente(@id as int)
RETURNS varchar(50)
AS
BEGIN
	DECLARE @RESULT as Varchar(50);
	SELECT @RESULT = CONCAT(first_name,' ',last_name) FROM sales.customers
	WHERE customer_id = @id;

	RETURN @RESULT;
END

/** Refactorizar ***/
GO
CREATE FUNCTION sales.nombre_cliente_table(@id as int)
RETURNS TABLE
AS
	RETURN  (SELECT  CONCAT(first_name,' ',last_name) nombre_completo FROM sales.customers
	WHERE customer_id = @id);
