

/** Cast tiene mejor rendimiento que la instruccion Convert**/
DECLARE @int as int = 1;

DECLARE @varchar varchar(4) = '10.0';

SELECT  CAST(@varchar as money) + 0.1 + @int;

SELECT CAST(@varchar + '12' as money) + 0.1 + @int;

SELECT CONVERT(money,@varchar) +  0.1 + @int;

SELECT CONVERT(varchar, CONVERT(money,@varchar) +  0.1 + @int,105);


/* Usando instruccion Convert para dar formato**/
DECLARE @fecha as DateTime = GETDATE();

SELECT @fecha,
	   CONVERT(Varchar,@Fecha,112),
	   CONVERT(Varchar,@Fecha,2),
	   CONVERT(Varchar,@Fecha,102)
