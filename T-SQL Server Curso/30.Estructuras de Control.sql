


If(1=0) /** Ejecuta instrucciones dentro del cuerpo si se cumple la expresion booleana **/
Begin /* Define un bloque de instrucciones T-SQL**/
	SELECT *,iif(order_id=1,'verdadero','false')
	FROM sales.orders;

	SELECT 0 as [Return];
End



/************ WHILE *****************/
DECLARE @contador as int = 0;
While 1=1
BEGIN
	If (@contador <= 5)
		SELECT @contador as [Return];
	Else
		BREAK;
	SET @contador += 1;
END
PRINT @contador;
GO
/*********** WHILE + CONTINUE **************/

DECLARE @contador as int = 0;
While @contador <=10
BEGIN
	SET @contador += 1;
	If (@contador > 5)
		Continue;

	SELECT @contador as [Return];
END
PRINT @contador;
GO

ALTER PROC dbo.GenerarFechas (@dias as int)
AS
BEGIN
/**** CALCULAR 30 Dias del Mes ****/
/**
crear tabla temporal/Variable tipo tabla
crear variable = 0
creando un while donde variable < 30
insertar valores en la tabla temporal
-Formato de dia semana
**/
SET LANGUAGE Spanish
SET DATEFIRST 1 /** Establece el primer dia de la semana*/
/**
	Lunes = 1
	Martes = 2
	Miercoles = 3
	Jueves = 4
	Viernes = 5
	Sabado = 6
	Domingo = 7
**/

DECLARE @fechas AS TABLE (Id int, Fecha datetime, DiaSemana Varchar(20));
DECLARE @i AS int = 0, @fechaInicial as DateTime = GETDATE();

While (select count(*) FROM @fechas) < @dias
BEGIN
	--INSERT INTO @fechas
	----VALUES(@i, DATEADD(dd,@i, @fechaInicial), DATENAME(dw, DATEADD(dd,@i, @fechaInicial)))
	--SELECT @i, DATEADD(dd,@i, @fechaInicial), DATENAME(dw, DATEADD(dd,@i, @fechaInicial))

	--INSERT INTO @fechas (id,fecha)
	--VALUES(@i, @fechaInsertar);

	--UPDATE @fechas SET DiaSemana = DATENAME(dw,fecha) WHERE Id = @i;

	DECLARE @fechaInsertar AS DATETIME = DATEADD(dd,@i, @fechaInicial);

	IF ( DATEPART(dw,@fechaInsertar) < 6)
		INSERT INTO @fechas
		VALUES(@i,@fechaInsertar, DATENAME(dw, @fechaInsertar))
	--ELSE
	--	SET @dias +=1;

	SET @i+=1;
END

SELECT * FROM @fechas;
SELECT id,fecha FROM @fechas;
END 

GO

CREATE FUNCTION dbo.diasSemana(@dias as int)
RETURNS  @fechas TABLE (Id int, Fecha datetime, DiaSemana Varchar(20))
AS
BEGIN

DECLARE @i AS int = 0, @fechaInicial as DateTime = GETDATE();

While @i < @dias
BEGIN

	DECLARE @fechaInsertar AS DATETIME = DATEADD(dd,@i, @fechaInicial);

	IF ( DATEPART(dw,@fechaInsertar) < 6)
		INSERT INTO @fechas
		VALUES(@i,@fechaInsertar, DATENAME(dw, @fechaInsertar))
	ELSE
		SET @dias +=1;

	SET @i+=1;
END
	RETURN 
END 