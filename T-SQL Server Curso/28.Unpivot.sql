
/***** UNPIVOT *****/
SELECT *,
	CASE Mes
		WHEN 'Enero' THEN 1
		WHEN 'Febrero' THEN 1
		WHEN 'Marzo' THEN 1
		WHEN 'Abril' THEN 1
		ELSE 0
	END as Mes_
FROM 
(
	SELECT store_name,store_id,staff_id, Enero,Febrero,
	Marzo,Abril,Mayo,Junio,Julio 
	FROM ##orders
) t1
UNPIVOT
(Valor FOR Mes IN (Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio) ) as R
Order BY Mes

;