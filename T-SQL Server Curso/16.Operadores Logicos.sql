/** ALL **/

SELECT * 
FROM production.products
WHERE model_year <= ALL (
	SELECT a�o FROM (
		VALUES(2019),(2017),(2018) 
	) as t (a�o)
);


/** IN **/
SELECT * 
FROM production.products
WHERE model_year IN (
	SELECT a�o FROM (
		VALUES(2019),(2017),(2018) 
	) as t (a�o)
);

/** SOME|ANY **/
SELECT * 
FROM production.products
WHERE model_year >= ANY (
	SELECT a�o FROM (
		VALUES(2019),(2017),(2018) 
	) as t (a�o)
);
