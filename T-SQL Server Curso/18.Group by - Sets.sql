USE [BikeStores]
GO


ALTER VIEW [sales].[vw_reporte_de_ventas]
AS
SELECT 
	o.store_id,
	ISNULL(s.store_name,'east store') as store_name,
	b.brand_id,

	p.product_id,
	p.product_name,
	p.category_id,
	c.category_name,

	b.brand_name,
	SUM(i.quantity*i.list_price*(1-i.discount)) as SubTotal
FROM sales.orders as  o
INNER JOIN sales.order_items as i
ON o.order_id = i.order_id
LEFT JOIN sales.stores s
ON o.store_id = s.store_id
LEFT JOIN production.products as p
ON i.product_id = p.product_id
LEFT JOIN production.brands b
ON b.brand_id = p.brand_id
LEFT JOIN production.categories c
ON c.category_id = p.category_id
GROUP BY o.store_id,s.store_id,s.store_name,b.brand_id,	b.brand_name,
     p.product_id,p.product_name,p.category_id,c.category_name



/** ROLLUP **/

SELECT store_name,brand_name,category_name,
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas

GROUP BY ROLLUP (store_name,brand_name,category_name)
UNION ALL
SELECT store_name,brand_name,category_name,
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas
GROUP BY ROLLUP (brand_name,store_name,category_name)
UNION ALL
SELECT store_name,brand_name,category_name,
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas
GROUP BY ROLLUP (category_name,brand_name,store_name)
UNION ALL
SELECT store_name,brand_name,category_name,
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas
GROUP BY ROLLUP (category_name,store_name,brand_name);

/** CUBE **/
SELECT store_name,brand_name,category_name,
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas
GROUP BY CUBE (store_name,brand_name,category_name)

 
/** GROUPING SETS **/
SELECT

  CASE 
	WHEN store_name IS NULL AND brand_name IS NULL AND category_name IS NULL THEN 'Total General' 
	ELSE store_name 
  END as store_name,
  CASE 
	WHEN brand_name IS NULL AND category_name IS NULL THEN 'Total Sucursal' 
	ELSE brand_name 
  END as brand_name,
  category_name,
    
	Format(SUM(SubTotal),'C') as SubTotal
FROM sales.vw_reporte_de_ventas
GROUP BY GROUPING SETS 
((store_name,brand_name,category_name),
 (store_name,brand_name),
 (store_name),
 ()
)

/***EJERCICIO***/
/**
USANDO GROUPING SETS OBTENER EL MISMO RESULTADO QUE ROLLUP
USANDO OPERADORES NULL ESTABLECER EL NOMBRE CORRESPONDIENTE A CADA CELDA DE TOTAL
(TOTAL GENERAL), (TOTAL POR MARCA),(TOTAL POR SUCURSAL)
**/

