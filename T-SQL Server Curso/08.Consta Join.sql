
-- Comentario
/* Es es
un comentario
de bloque */

/* INNER JOIN*/
SELECT
	T1.Id, 
	T1.Value,
	T2.Id,
	T2.Value,
	T1.Value + T2.Value as Concatenacion, --Alias Concatenacion
	Concatenacion = T1.Value + T2.Value -- Alias Concatenacion
FROM T1 INNER JOIN T2 
ON T1.Id  = T2.Id 

/**LEFT JOIN**/
SELECT
	T1.Id, 
	T1.Value,
	T2.Id,
	T2.Value
FROM T1 
	LEFT JOIN T2 
	ON T1.Id  = T2.Id 

/**LEFT RIGHT**/
SELECT
	T1.Id, 
	T1.Value,
	T2.Id,
	T2.Value
FROM T1 
	RIGHT JOIN T2 
	ON T1.Id  = T2.Id 

/**FULL JOIN**/
SELECT
	*
FROM T1 
	FULL JOIN T2 
	ON T1.Id  = T2.Id AND T1.id > 5

/*Cross Join*/
SELECT
	*
FROM T1 
CROSS JOIN T2 

/* Guardar Resultado Escalar en una variable*/
DECLARE @Combinaciones int = (SELECT
	Count(*)
FROM T1 
CROSS JOIN T2 );

Print @Combinaciones;

GO

DECLARE @Combinaciones int = 0 ; 

SELECT
	@Combinaciones += T1.Id,
	@Combinaciones = @Combinaciones + T1.id
FROM T1 
CROSS JOIN T2;

Print @Combinaciones;
