USE TestDB

SELECT * FROM T1 INNER JOIN T2 ON t1.Id = t2.Id

SELECT * FROM T2

UPDATE T2 
SET value = 'r'
OUTPUT 
deleted.Id,deleted.Value,inserted.Value
INTO #tmp
;


/*Usando Instruccion OUTPUT*/
UPDATE T2	
	Set Value =  LOWER(T1.Value) 
	OUTPUT 
		deleted.Id,inserted.Id,deleted.Value old_value,inserted.Value as new_value
	FROM T2 INNER JOIN T1 
	ON T2.Id = T1.Id
	

/*OUTPUT - INTO*/
CREATE TABLE #tmp (
	Id int,
	old_value varchar(10),
	new_value varchar(10)
);

UPDATE T2 
SET value = 'r'
OUTPUT 
deleted.Id,deleted.Value,inserted.Value
INTO #tmp;

UPDATE T2	
	Set Value =  LOWER(T1.Value) 
	OUTPUT 
		deleted.Id,deleted.Value old_value,inserted.Value as new_value
	INTO #tmp
	FROM T2 INNER JOIN T1 
	ON T2.Id = T1.Id

SELECT * FROM #tmp;