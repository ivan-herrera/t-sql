CREATE TABLE #Tabla1 (id int, valor varchar(20));

INSERT INTO #Tabla1 
VALUES(1,'Ivan'),(2,'Juan'),(3,'Luis'),(4,'Esteban'),(5,'Hugo')

SELECT * FROM #Tabla1;



CREATE TABLE #Tabla2 (id int, valor varchar(20));

INSERT INTO #Tabla2 
VALUES(1,'Adolfo'),(2,'Juan'),(3,'Luis'),(4,'Esteban'),(6,'Hugo')

/**** UNION ******/
SELECT * FROM #Tabla1
UNION
SELECT * FROM #Tabla2

/**** UNION ALL ******/
SELECT * FROM #Tabla1
UNION ALL
SELECT * FROM #Tabla2

/*** INTERSECT ***/
SELECT * FROM #Tabla1
INTERSECT
SELECT * FROM #Tabla2

/*** EXCEPT ***/
SELECT * FROM #Tabla1
EXCEPT
SELECT * FROM #Tabla2


/**Consultar todas los renglones diferentes*/
SELECT * FROM
	(SELECT * FROM #Tabla1
	EXCEPT
	SELECT * FROM #Tabla2) as T1
UNION
SELECT * FROM
	(SELECT * FROM #Tabla2
	EXCEPT
	SELECT * FROM #Tabla1)as T2