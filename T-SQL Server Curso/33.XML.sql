/**** GENERAR XML ****/

SELECT TOP 3 
	customer.customer_id,first_name,email
--order_date,order_id,order_status
FROM sales.customers customer 
--INNER JOIN sales.orders orders
--on orders.customer_id = customer.customer_id
FOR XML Raw('customer'), Root('customers'),Elements

/** LEER XML - Propiedades**/

DECLARE @xml as varchar(MAX) = '
<customers>
  <customer customer_id="1" first_name="Debra" email="debra.burks@yahoo.com" />
  <customer customer_id="2" first_name="Kasha" email="kasha.todd@yahoo.com" />
  <customer customer_id="3" first_name="Tameka" email="tameka.fisher@aol.com" />
</customers>';

DECLARE @iDoc as int;

EXEC sp_xml_preparedocument @iDoc OUT,@xml;

SELECT * FROM 
OPENXML (@iDoc,'/customers/customer',1)
	WITH( customer_id int,
		  first_name varchar(30),
		  email varchar(50))
;

EXEC sp_xml_removedocument @iDoc;

GO
/**********************/
/** LEER XML - Elementos **/

DECLARE @xml as varchar(MAX) = '
<customers>
  <customer>
    <customer_id>1</customer_id>
    <first_name>Debra</first_name>
    <email>debra.burks@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>2</customer_id>
    <first_name>Kasha</first_name>
    <email>kasha.todd@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>3</customer_id>
    <first_name>Tameka</first_name>
    <email>tameka.fisher@aol.com</email>
  </customer>
</customers>';

DECLARE @iDoc as int;

EXEC sp_xml_preparedocument @iDoc OUT,@xml;

INSERT INTO dbo.customers
SELECT *,CAST(customer_id as bigint) as id FROM 
OPENXML (@iDoc,'/customers/customer',2)
	WITH( customer_id int,
		  first_name varchar(30),
		  email varchar(50))
;

EXEC sp_xml_removedocument @iDoc; --Libera memoria


--DROP TABLE IF EXISTS  dbo.customers

GO

/**************************/
/** LEER XML - Elementos **/

DECLARE @xml as varchar(MAX) = '
<customers store_id="1">
  <name>Suc 1</name>
  <customer id="1">
    <customer_id>1</customer_id>
    <first_name>Debra</first_name>
    <email>debra.burks@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>2</customer_id>
    <first_name>Kasha</first_name>
    <email>kasha.todd@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>3</customer_id>
    <first_name>Tameka</first_name>
    <email>tameka.fisher@aol.com</email>
  </customer>
</customers>';

DECLARE @iDoc as int;

EXEC sp_xml_preparedocument @iDoc OUT,@xml;


SELECT * FROM 
OPENXML (@iDoc,'/customers/customer',3)
	WITH( customer_id int,
		  first_name varchar(30),
		  email varchar(50),
		  id bigint,
		  store_id  int '../@store_id',
		  [name] varchar(30) '../name')
;

EXEC sp_xml_removedocument @iDoc; --Libera memoria


/*** OPENXML (id Documento ,elemento raiz de donde iniciar la iteración, Formato de los datos) ****/
/*********
Formato
1: Formato Datos como Propiedad/Atributo
2: Formato Datos como Elemento
3: Formato Datos como Elemento/Propiedad/Atributo
*********/

GO
/*********************/
DECLARE @xml as xml = '
<customers>
  <customer>
    <customer_id>1</customer_id>
    <first_name>Debra</first_name>
    <email>debra.burks@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>2</customer_id>
    <first_name>Kasha</first_name>
    <email>kasha.todd@yahoo.com</email>
  </customer>
  <customer>
    <customer_id>3</customer_id>
    <first_name>Tameka</first_name>
    <email>tameka.fisher@aol.com</email>
  </customer>
</customers>';

select  @xml.query('/customers/customer') as t;