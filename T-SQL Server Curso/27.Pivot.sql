
/******* Pivot *********/
SELECT s.store_name, orders.* FROM
(
SELECT order_id,store_id,staff_id,
	Month(order_date) as  Mes
FROM sales.orders
) as Tg
PIVOT
(COUNT(order_id) FOR Mes IN ([1],[2],[3],[4],[5],[6])) AS Orders
INNER JOIN sales.stores s
ON s.store_id = Orders.store_id

/**** Pivot 2  ***/
SET LANGUAGE SPANISH

SELECT s.store_name, orders.* INTO ##orders FROM
(
SELECT order_id,store_id,staff_id,
	DATENAME(MM,order_date) as  Mes
FROM sales.orders
) as Tg
PIVOT
(COUNT(order_id) FOR Mes IN ([Enero],[Febrero],[Marzo],[Abril],[Mayo],[Junio],[Julio])) AS Orders
INNER JOIN sales.stores s
ON s.store_id = Orders.store_id

/********/
/*****
	Crear una tabla de presupuestos de venta por sucursal-Mes
	Subtotal * 1.3
	store_id,year,ene,feb,mar.....
***/
GO
CREATE FUNCTION production.fnCalcularSubTotal(@quantity int,@list_price money, @discount money)
RETURNS Money
AS 
BEGIN
	DECLARE @result as money = @quantity * @list_price * (1-@discount);
	RETURN @result;
END
GO

SELECT *,2020 as [year] INTO sales.budgets 
FROM (
	SELECT store_id, --Lower(Format(order_date,'MMM','es-MX')),
		CHOOSE(Month(order_date),'ene','feb','mar','abr','may','jun','jul','ago','sep','oct','nov','dic' ) as mes,
	SUM(quantity * list_price * (1-discount))*1.3 as SubTotal
	FROM 
	sales.orders o INNER JOIN sales.order_items i
	ON o.order_id = i.order_id
	GROUP BY store_id,Year(order_date),Month(order_date)--,Format(order_date,'MMM','es-MX')
) AS [TO]
PIVOT
(AVG(SubTotal) FOR mes IN (ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic) ) AS R1
GO
