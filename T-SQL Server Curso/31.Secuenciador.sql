USE [BikeStores]

GO

CREATE SEQUENCE [sales].[Foliador] 
 AS [tinyint]
 START WITH 0
 INCREMENT BY 1
 MINVALUE 0
 MAXVALUE 10
 CYCLE 

GO

CREATE SEQUENCE [sales].[Foliador2] 
 AS [tinyint]
 START WITH 0
 INCREMENT BY 1
 MINVALUE 0
 MAXVALUE 10
 NO CYCLE 

GO


DECLARE @var as int;

SET @var = NEXT VALUE FOR sales.[Foliador2];

SELECT @var;

